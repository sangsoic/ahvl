# A handful generic vector C library

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

## About

* This C vector library is [generic](https://en.wikipedia.org/wiki/Generic_programming).
* You can access the project documentation within the Doc directory.
* You can also read the next section of this page in order to understand basic usage.
* If you would like to know more about the general concept of vectors [read this document](https://www.techopedia.com/definition/22817/vector-programming).
* Feel free to submit changes.

## Usage

There is two way to access routines. The easiest is using wrapper routine with macros and the more advanced way is to call the original generic version of the routine. The following sections cover each techniques in depth.

Vector typed objects contains 3 fields:

* value which is the address of the dynamically allocated array.
* cardinal which is the current number of value contained inside the vector.
* capacity which is the maximum number of value that the vector can currently contain.

### Wrapper routine

This method allows you to call macros to define specially typed structures and routines.

#### Macro syntax

```
VECTOR_[ROUTINE IDENTIFIER](<TYPE>, <NAME>)
```

Where *[ROUTINE IDENTIFIER]* is the upper case routine name, if no routine name specified then it declares the specially typed vector structure, *\<TYPE\>* the vector type, *\<NAME\>* an identifier.  
To learn more about each macro in documentation files.

#### Example

```C
#include <stdio.h>
#include "vector.h"

VECTOR(int, Int)
VECTOR_MALLOC(int, Int)
VECTOR_GET(int, Int)
VECTOR_SET(int, Int)
VECTOR_REALLOC(int, Int)
VECTOR_SHIFT_RIGHT(int, Int)
VECTOR_FREE(int, Int)

int main(void) {
	VectorInt * vector;
	vector = VectorInt_malloc(3);
	VectorInt_set(vector, 0, 11);
	VectorInt_set(vector, 1, 209);
	VectorInt_set(vector, 2, 133);
	vector->cardinal = 3;
	printf("%d\n", VectorInt_get(vector, 0));
	printf("%d\n", VectorInt_get(vector, 1));
	printf("%d\n", VectorInt_get(vector, 2));
	vector = VectorInt_realloc(vector, vector->capacity + 1);
	VectorInt_shift_right(vector);
	VectorInt_free(&vector);
	return 0;
}
```

### Generic routine

This method allows you to call generic routine directly, however you will need to handle casting manually.  
To learn more about each generic routine you can read the [documentation][documentation]

#### Exemple

```C
#include <stdio.h>
#include "vector.h"

int main(void) {
	Vector * vector;
	vector = Vector_malloc(3, sizeof(int));
	((int *)vector->value)[0] = 11;
	((int *)vector->value)[1] = 209;
	((int *)vector->value)[2] = 133;
	vector->cardinal = 3;
	printf("%d\n", ((int *)vector->value)[0]);
	printf("%d\n", ((int *)vector->value)[1]);
	printf("%d\n", ((int *)vector->value)[2]);
	vector = Vector_realloc(vector, vector->capacity + 1, sizeof(int));
	vector->cardinal++;
	Vector_shift_right(vector, sizeof(int));
	Vector_free(&vector);
	return 0;
}
```

### Sub Vectors

Sub vectors are used as an indexed access structure to a vector. They allow to manipulate all sub vectors of any vector. Furthermore, sub vectors do not require to allocate or copy more vectors in memory as they are only pointing the values of a parent vector in a certain order and frequency.
The sub vector structure is composed of the 3 following fields :

* startAt, which consists of a pointer containing the address of an element of a parent vector.
* index, which is any sequence that maps the set of natural numbers {0, 1, 2, ...} to a set of integers.
* length, which is the number of elements pointed by the sub vector.

#### Exemple

```C
#include <stdio.h>
#include <stdlib.h>
#include "vector.h"
#include "subvector.h"

SUBVECTOR(int, Int)
VECTOR(int, Int)

VECTOR_MALLOC(int, Int)
VECTOR_FREE(int, Int)

SUBVECTOR_EXTRACT(int, Int)
SUBVECTOR_GET(int, Int)
SUBVECTOR_FREE(int, Int)

ssize_t idx(const size_t i) { return i * (-5); }

int main(void)
{
	VectorInt * vector;
	SubVectorInt * sub;
	vector = VectorInt_malloc(101);
	for (int i = 0; i <= 100; i++) {
		vector->value[i] = i;
	}
	sub = SubVectorInt_extract(100, idx, 21, vector, NULL);
	printf("SUBVECTOR VALUES:\n");
	for (int i = 0; i < sub->length; i++) {
		printf("%d\n", SubVectorInt_get(sub, i));
	}
	printf("LENGTH\n");
	printf("%lu\n", sub->length);
	VectorInt_free(&vector);
	SubVectorInt_free(&sub);
	return 0;
}
```

Which outputs : 

```
SUBVECTOR VALUES:
100
95
90
85
80
75
70
65
60
55
50
45
40
35
30
25
20
15
10
5
0
LENGTH:
21
```
