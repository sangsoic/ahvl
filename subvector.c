#include "subvector.h"

static SubVector * SubVector_malloc(void)
{
	SubVector * sub;
	if ((sub = malloc(sizeof(SubVector))) == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return sub;
}

void SubVector_free(SubVector * * const sub)
{
	free(*sub);
	*sub = NULL;
}

SubVector * SubVector_extract(const size_t start, ssize_t (* index)(const size_t), const size_t length, const Vector * const from, SubVector * to, const size_t offset)
{
	free(to);
	to = SubVector_malloc();
	to->startAt = Vector_at(from, start, offset);
	to->index = index;
	to->length = length;
	return to;
}

void * SubVector_at(const SubVector * const sub, const size_t i, const size_t offset)
{
	return ((unsigned char *)sub->startAt) + sub->index(i) * offset;
}

SubVector * SubSubVector_extract(const size_t start, ssize_t (* index)(const size_t), const size_t length, const SubVector * const from, SubVector * to, const size_t offset)
{
	free(to);
	to = SubVector_malloc();
	to->startAt = SubVector_at(from, start, offset);
	to->index = index;
	to->length = length;
	return to;
}
